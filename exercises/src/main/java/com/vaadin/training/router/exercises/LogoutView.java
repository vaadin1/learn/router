package com.vaadin.training.router.exercises;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;

import java.util.Optional;

@Route(value = "/logout", layout = MainView.class)
public class LogoutView extends Composite<Div> implements BeforeEnterObserver {

    public LogoutView() {
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        VaadinSession.getCurrent().setAttribute("userLoggedIn", null);
        VaadinSession.getCurrent().getSession().invalidate();
        UI.getCurrent().getPage().setLocation("login");
    }
}
