package com.vaadin.training.router.exercises;

import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.VaadinSession;

public class MainView extends HorizontalLayout implements RouterLayout, BeforeEnterObserver {
    private Div childWrapper = new Div();

    public MainView() {
        VerticalLayout verti1 = new VerticalLayout();
        verti1.add(new RouterLink("Home", HomeView.class));
        verti1.add(new RouterLink("Lottery", LotteryView.class, 1));
        verti1.add(new RouterLink("Logout", LogoutView.class));

        VerticalLayout verti2 = new VerticalLayout();
        verti2.add(new H1("Header"));
        verti2.add(childWrapper);
        verti2.add(new H1("Footer"));

        verti1.setWidth("10%");
        verti2.setWidth("90%");
        add(verti1, verti2);
    }

    @Override
    public void showRouterLayoutContent(HasElement content) {
        childWrapper.getElement().appendChild(content.getElement());
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        if (VaadinSession.getCurrent().getAttribute("userLoggedIn") == null) {
            VaadinSession.getCurrent().setAttribute("intendedPath", event.getLocation().getPath());
            event.forwardTo(LogoutView.class);
        }
    }
}
