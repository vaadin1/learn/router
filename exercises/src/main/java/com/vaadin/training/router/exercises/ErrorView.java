package com.vaadin.training.router.exercises;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.router.*;

@Route("/err")
public class ErrorView extends Div implements HasErrorParameter<InvalidValueException> {

    @Override
    public int setErrorParameter(BeforeEnterEvent event, ErrorParameter<InvalidValueException> parameter) {
        add(new Span("Ooops! invalid number"));
        return 500;
    }
}
